package com.company;

import java.io.IOException;
import java.net.Socket;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.*;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.nio.file.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;


//sql import

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class WorkerRunnable implements Runnable {


    protected Socket clientSocket = null;
    protected String serverText = null;


    // username and password of MySQL server
    private static final String url = "jdbc:mysql://185.58.205.8:3306/triwizard?useSSL=false";
    private static final String user = "root";
    private static final String password = "1q22w3e4";


    // variables for opening and managing connection
    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;


    /**
     * Seter for head fields of class
     *
     * @param clientSocket - client socket (connection chanel)
     * @param serverText   - name of server (if it needs)
     */


    public WorkerRunnable(Socket clientSocket, String serverText) {
        this.clientSocket = clientSocket;
        this.serverText = serverText;
    }

    /**
     *  Method print String in console
     * @param userName - user name
     * @param action - action of server
     */
    public void loger(String userName , String action)
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println("["+dateFormat.format(date)+"]["+userName+"] - ["+action+"]");
    }

    /**
     * Method check mysql connection
     */
    public void checkMSQLConnection() {
        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(url, user, password);

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }


    /**
     * Method update count of user Goblets
     * @param userName - user name
     * @param Score - count of Goblet to update
     */
    public void updateUserGoblet(String userName, Integer Score)
    {

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String sql = "UPDATE Rating " +
                    "SET GamesWon = "+ Score +" WHERE User='" + userName + "'" ;
            stmt.executeUpdate(sql);

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

    }

    /**
     * Method get user count of Goblet
     * @param userName - user name
     * @return count of Goblet
     */
    public Integer getUserGoblet(String userName) {

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String query = "SELECT GamesWon FROM UserStat WHERE User='" + userName + "'";

            // executing SELECT query

            rs = stmt.executeQuery(query);

            if (rs.next()) {
                Integer name = rs.getInt(1);
                return name;
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        return 0;

    }

    /**
     * Method get user folder name
     *
     * @param userName - name of user
     * @return folder name
     */
    public String getUserFolder(String userName) {

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String query = "SELECT Folder FROM Users WHERE User='" + userName + "'";

            // executing SELECT query

            rs = stmt.executeQuery(query);

            if (rs.next()) {
                String name = rs.getString(1);
                return name;
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        return "";

    }


    /**
     * Method gets Top user from db
     *
     * @return HashMap with Top users
     */
    public HashMap<String, Integer> getTop() {

        HashMap<String, Integer> map = new HashMap<String, Integer>();

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String query = "select User, HighScores from UserStat";


            rs = stmt.executeQuery(query);

            while (rs.next()) {
                String name = rs.getString(1);
                String score = rs.getString(2);
                int value=0;

                    int spaceIndex = score.indexOf("/");

                    if (spaceIndex != -1)
                    {
                        score = score.substring(0, spaceIndex);
                    }
                    value = Integer.parseInt(score);

                map.put(name, value);
            }

            return sortByValues(map);


        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        return sortByValues(map);
    }

    /**
     * Method for sort HashMap
     *
     * @param map - input map
     * @return sorted map
     */
    private static HashMap sortByValues(HashMap map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }

    /**
     * Method checks existing of user in db
     *
     * @param userName - name of user
     * @return true - user exist , false - user not found
     */
    public boolean CheckUser(String userName) {

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String query = "SELECT * FROM Users WHERE User='" + userName + "'";

            // executing SELECT query

            rs = stmt.executeQuery(query);

            if (rs.next()) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }


        return false;

    }

    /**
     * Method add user in db
     *
     * @param userName   - name of user
     * @param FolderName - generated folder name
     */
    public void addUser(String userName, String FolderName) {
        try {
            // getting Statement object to execute query

            if (CheckUser(userName)) {
                return;
            }
            stmt = con.createStatement();

            String query = "INSERT INTO triwizard.Users (User, Folder,Online) \n" +
                    " VALUES ('" + userName + "','" + FolderName + "',0);";
            // executing SELECT query
            stmt.executeUpdate(query);

             query = "INSERT INTO triwizard.UserStat (User) \n" +
                    " VALUE ('" + userName + "');";
            // executing SELECT query
            stmt.executeUpdate(query);

            String sql = "UPDATE UserStat " +
                    "SET HighScores = '0',WizardKill = 0 , MonsterKill = 0 , GamesWon = 0 , FirstAvatar = 0 , SecondAvatar = 0 , ThirdAvatar = 0 , FourAvatar = 0 WHERE User='" + userName + "'" ;
            stmt.executeUpdate(sql);


        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

    }

    /**
     * Method create MD5 hash for name of user folder
     *
     * @param md5 - unique string
     * @return
     */
    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    /**
     *  Method get invites list from db
     * @param userName - name of user
     * @return arraylist with invites
     */
    public ArrayList<String> getInvite(String userName) {

        ArrayList<String> invites = new ArrayList<String>();

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String query = "select User2 WHERE User1 = '"+userName+"' from Invite";


            rs = stmt.executeQuery(query);

            while (rs.next()) {
                String player = rs.getString(1);
                invites.add(player);
            }

            return invites;


        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        return invites;
    }


    /**
     *  Method get online users
     * @return arraylist with online users
     */
    public ArrayList<String> getOnlineUsers() {

        ArrayList<String> players = new ArrayList<String>();

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String query = "select User WHERE Online = TRUE from Users";


            rs = stmt.executeQuery(query);

            while (rs.next()) {
                String player = rs.getString(1);
                players.add(player);

            }

            return players;


        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        return players;
    }

    /**
     *  Method get user history matches
     * @param userName - name of user
     * @return arraylist with history
     */
    public ArrayList<String> getHistory(String userName) {

        ArrayList<String> History = new ArrayList<String>();

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String query = "select User2 WHERE User1 = '"+userName+"' from OnlineGames";


            rs = stmt.executeQuery(query);

            while (rs.next()) {
                String player = rs.getString(1);
                History.add(player);

            }

            return History;


        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        return History;
    }

    /**
     *  Method set online flag in database
     * @param user - name of user
     * @param flag - true(online)/false(offline)
     * @return success of operation
     */
    public boolean setOnlineStatus(String user , boolean flag)
    {

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String sql = "UPDATE Users " +
                    "SET Online = "+ flag +" WHERE User='" + user + "'" ;
            stmt.executeUpdate(sql);

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return false;
    }

    /**
     *  Method add invite in db
     * @param first - from user
     * @param second - to user
     * @return success of operation
     */
    public boolean addInvite (String first ,String second)
    {

        try {
            // getting Statement object to execute query

            if (CheckUser(first) || CheckUser(second)) {
                return false;
            }

            stmt = con.createStatement();

            String query = "INSERT INTO triwizard.Invite (User1, User2 , Status) \n" +
                    " VALUES ('" + first + "','" + second + "',0);";
            // executing SELECT query
            stmt.executeUpdate(query);

            return  true;

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        return false;
    }


    /**
     *  Method update user stat in database
     * @param userName - name of user
     * @param HighScore - 10 highscores
     * @param WizardKill - count of wizard kills
     * @param MonsterKill - count of monster kills
     * @param GamesWon - count of games won matches
     * @param FirstAvatar - win with first avatar
     * @param SecondAvatar - win with second avatar
     * @param ThirdAvatar - win with third avatar
     * @param FourAvatar - win with four avatar
     */
    public void updateUserStat(String userName, String HighScore , int WizardKill, int MonsterKill, int GamesWon, int FirstAvatar
    , int SecondAvatar , int ThirdAvatar , int FourAvatar)
    {

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String sql = "UPDATE UserStat " +
                    "SET HighScores = '"+ HighScore +  "',WizardKill = "+WizardKill +"+, MonsterKill ="+MonsterKill+" , GamesWon = "+GamesWon+" , FirstAvatar = "+FirstAvatar+" , SecondAvatar ="+SecondAvatar+" , ThirdAvatar ="+ThirdAvatar+" , FourAvatar = "+FourAvatar+" WHERE User='" + userName + "'" ;
            stmt.executeUpdate(sql);

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

    }

    /**
     *  Method get user stat from db
     * @param username - name of user
     * @return arraylist with user stat
     */
    public  ArrayList<String> getUserStat(String username)
    {
        ArrayList<String> UserStat = new ArrayList<String>();

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String sql = "select HighScores ,WizardKill , MonsterKill  , GamesWon , FirstAvatar , SecondAvatar , ThirdAvatar  , FourAvatar  WHERE User='" + username + "' from UserStat" ;


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                //Integer.toString(number)
                UserStat.add(rs.getString(1));
                UserStat.add(Integer.toString(rs.getInt(2)));
                UserStat.add(Integer.toString(rs.getInt(3)));
                UserStat.add(Integer.toString(rs.getInt(4)));
                UserStat.add(Integer.toString(rs.getInt(5)));
                UserStat.add(Integer.toString(rs.getInt(6)));
                UserStat.add(Integer.toString(rs.getInt(7)));
                UserStat.add(Integer.toString(rs.getInt(8)));

            }

            return UserStat;


        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        return UserStat;


    }


    /**
     *  Method send file by socket
     * @param file - path to file
     * @throws IOException result of operation
     */
    public void sendFile(String file) throws IOException {
        DataOutputStream dos = new DataOutputStream(clientSocket.getOutputStream());
        FileInputStream fis = new FileInputStream(file);
        byte[] buffer = new byte[4096];

        while (fis.read(buffer) > 0) {
            dos.write(buffer);
        }

        fis.close();
        dos.close();
    }

    /**
     *  Method saves file by socket
     * @param path - path to file
     * @param filesize - size of file
     * @throws IOException result of operation
     */
    private void saveFile(String path , int filesize) throws IOException {
        DataInputStream dis = new DataInputStream(clientSocket.getInputStream());
        FileOutputStream fos = new FileOutputStream(path);
        byte[] buffer = new byte[4096];

        int read = 0;
        int totalRead = 0;
        int remaining = filesize;
        while((read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
            totalRead += read;
            remaining -= read;
            System.out.println("read " + totalRead + " bytes.");
            fos.write(buffer, 0, read);
        }

        fos.close();
        dis.close();
    }

    /**
     * Run method of new thread
     */
    public void run() {
        try {
            InputStream input = clientSocket.getInputStream();
            OutputStream output = clientSocket.getOutputStream();

            DataInputStream in = new DataInputStream(input);
            DataOutputStream out = new DataOutputStream(output);

            //ObjectInputStream objectIn = new ObjectInputStream(input);
            //ObjectOutputStream objectOut = new ObjectOutputStream(output);

            checkMSQLConnection();

            Integer command = null;
            command = in.readInt();

            switch (command) {
                case 100 : // ath
                    String login = null;
                    login = in.readUTF();
                    if (!CheckUser(login)) {
                        addUser(login, MD5(login));
                        loger(login,"Create new user");
                        out.writeInt(101);
                        out.flush();
                    } else {
                        loger(login,"This user exist");
                        out.writeInt(102);
                        out.flush();
                    }
                    break;

                case 200:
                    login = null;
                    login = in.readUTF();

                    Integer oldScore = getUserGoblet(login);
                    loger(login,"Get user count of Goblet");
                    updateUserGoblet(login,oldScore+1);
                    loger(login,"Update count of Goblet");
                    if (oldScore+1 == getUserGoblet(login))
                    {
                        loger(login,"Success updating count of Goblet");
                        out.writeInt(201);
                        out.flush();
                    }
                    else{
                        loger(login,"Failed updating count of Goblet");
                        out.writeInt(202);
                        out.flush();
                    }

                    break;

                case 300:
                    login = null;
                    login = in.readUTF();
                    Integer goblet = getUserGoblet(login);
                    loger(login,"Get user count of Goblet");
                    out.writeInt(goblet);
                    out.flush();

                    break;

                case 400: // get top player

                    HashMap<String, Integer> map = getTop();
                    loger("","Get top users");
                    Integer count = 0;
                    out.writeInt(map.size());
                    out.flush();

                    for (Map.Entry<String, Integer> entry : map.entrySet()) {
                        out.writeUTF(entry.getKey());
                        out.flush();
                        out.writeInt(entry.getValue());
                        out.flush();
                        count++;
                        if(count==5){ break;}
                    }
                    map.clear();
                    break;

                case 700:
                    login = in.readUTF();
                    boolean flag = in.readBoolean();

                    loger(login,"Set"+ flag + "to online");

                    out.writeBoolean(setOnlineStatus(login,flag));
                    out.flush();
                    break;

                case 701:
                    loger("","Get online users");
                    ArrayList<String> players = getOnlineUsers();
                    out.writeInt(players.size());
                    out.flush();

                    for(String state : players){
                        out.writeUTF(state);
                        out.flush();
                    }
                    players.clear();
                    break;


                case 702:

                    String loginFisrt = in.readUTF();
                    String loginSecond = in.readUTF();

                    loger(loginFisrt +" "+ loginSecond,"Create invite");
                     out.writeBoolean(addInvite(loginFisrt,loginSecond));
                     out.flush();

                    break;

                case 703:
                    login = in.readUTF();
                    loger(login,"Get users invite");
                    ArrayList<String> invites = getInvite(login);
                    out.writeInt(invites.size());
                    out.flush();

                    for(String state : invites){
                        out.writeUTF(state);
                        out.flush();
                    }
                    invites.clear();
                    break;

                case 704:


                    break;

                case 705:
                    login = in.readUTF();
                    loger(login,"Get user history of matches ");
                    ArrayList<String> history = getHistory(login);
                    out.writeInt(history.size());
                    out.flush();

                    for(String state : history){
                        out.writeUTF(state);
                        out.flush();
                    }
                    history.clear();
                    break;

                case 706:
                    login = null;
                    login = in.readUTF();
                    loger(login,"Update user stats");
                    updateUserStat(login, in.readUTF() , in.readInt(), in.readInt(), in.readInt(),in.readInt()
    , in.readInt() ,in.readInt(), in.readInt());

                    break;

                case 707:

                    login = in.readUTF();
                    loger(login,"Get user stats");
                    ArrayList<String> userStat = getUserStat(login);
                     count = 0 ;
                    for(String state : userStat){
                        if(count > 0)
                        {
                            out.writeInt( Integer.parseInt(state));
                            out.flush();
                        }
                        else {
                            out.writeUTF(state);
                            out.flush();
                        }
                        count++;
                    }
                    userStat.clear();
                    break;

                case 708:
                    login = null;
                    login = in.readUTF();


                    loger(login,"Check save on server");

                    String folder = getUserFolder(login);

                    loger(login,"Get folder name ");

                    Path currentRelativePath = Paths.get("");
                    String s = currentRelativePath.toAbsolutePath().toString();

                    s+="/Saves/" + folder +"/Save.cer";


                    Path path = Paths.get(s);
                    if (Files.exists(path))
                    {
                        try {
                            Files.delete(path);
                        } catch (NoSuchFileException x) {
                            System.err.format("%s: no such" + " file or directory%n", path);
                        } catch (DirectoryNotEmptyException x) {
                            System.err.format("%s not empty%n", path);
                        } catch (IOException x) {
                            // File permission problems are caught here.
                            System.err.println(x);
                        }

                    }

                    loger(login,"Delete save file");
                    break;

                case 709:

                    login = null;
                    login = in.readUTF();


                    loger(login,"Check save on server");

                     folder = getUserFolder(login);

                    loger(login,"Get folder name ");

                     currentRelativePath = Paths.get("");
                     s = currentRelativePath.toAbsolutePath().toString();

                    s+="/Saves/" + folder +"/Save.cer";


                     path = Paths.get(s);
                    if (Files.exists(path))
                    {
                        try {
                            Files.delete(path);
                        } catch (NoSuchFileException x) {
                            System.err.format("%s: no such" + " file or directory%n", path);
                        } catch (DirectoryNotEmptyException x) {
                            System.err.format("%s not empty%n", path);
                        } catch (IOException x) {
                            // File permission problems are caught here.
                            System.err.println(x);
                        }


                    }

                    int filesize = in.readInt();

                    saveFile(s,filesize);


                    loger(login,"save save file");
                    break;

                case 710:
                    login = null;
                    login = in.readUTF();
                    String time = null;
                    time = in.readUTF();

                    loger(login,"Check save on server");

                    folder = getUserFolder(login);

                    loger(login,"Get folder name ");

                    currentRelativePath = Paths.get("");
                    s = currentRelativePath.toAbsolutePath().toString();

                    s+="/Saves/" + folder +"/Save.cer";


                    path = Paths.get(s);
                    if (Files.exists(path)) {

                        BasicFileAttributes attr;
                        try {
                            attr = Files.readAttributes(path, BasicFileAttributes.class);
                            //(1970-01-01T00:00:00Z)
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-DDThh:mm:ss[.s+]Z", Locale.ENGLISH);
                            LocalDate dateServer = LocalDate.parse(attr.creationTime().toString(), formatter);
                            LocalDate dateClient = LocalDate.parse(time, formatter);

                            int result = dateServer.compareTo(dateClient);

                            out.writeInt(result);
                            out.flush();
                            if(result < 0)
                            {
                                try {
                                    Files.delete(path);
                                } catch (NoSuchFileException x) {
                                    System.err.format("%s: no such" + " file or directory%n", path);
                                } catch (DirectoryNotEmptyException x) {
                                    System.err.format("%s not empty%n", path);
                                } catch (IOException x) {
                                    // File permission problems are caught here.
                                    System.err.println(x);
                                }

                                filesize = in.readInt();

                                saveFile(s,filesize);


                            }
                            else if (result == 0){

                            }
                            else {
                                out.writeInt((int)attr.size());
                                out.flush();
                                sendFile(s);

                            }


                        } catch (IOException e) {

                            System.out.println("oops error! " + e.getMessage());

                        }
                        // file exist
                    }
                    else{
                        out.writeInt(-1);
                        out.flush();
                        filesize = in.readInt();

                        saveFile(s,filesize);
                    }

                    break;

                case 711:
                    login = null;
                    login = in.readUTF();


                    loger(login,"Check save on server");

                     folder = getUserFolder(login);

                    loger(login,"Get folder name ");

                     currentRelativePath = Paths.get("");
                     s = currentRelativePath.toAbsolutePath().toString();

                    s+="/Saves/" + folder +"/Save.cer";


                     path = Paths.get(s);
                    if (Files.exists(path))
                    {
                        BasicFileAttributes attr;
                        attr = Files.readAttributes(path, BasicFileAttributes.class);
                        out.writeInt((int)attr.size());
                        out.flush();
                        sendFile(s);
                    }

                    loger(login,"load save file to client");
                    break;


                case 999:
                    loger("","Server shuts down");
                    System.exit(0);
                    break;


                default:
                    break;

            }
            in.close();
            out.close();


            output.close();
            input.close();

        } catch (IOException e) {
            //report exception somewhere.
            e.printStackTrace();
        }
    }
}


