/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlrequest;

/**
 *
 * @author godric
 */
public class RequestServer {
    static int RQ_AUTH = 100;
    public static int ANS_USERCREATE = 101;
    public static int ANS_USEREXIST  = 102;
    
    static int RQ_INC_GOBLET = 200;
    
    static int ANS_OK = 201;
    static int ANS_NOT_AUTH = 202;
    
    static int RQ_GET_GOBLET = 300 ;
    static int RQ_TOP_PLAYERS = 400;
    
    static int RQ_UPDATE_STAT = 706;
    static int RQ_GET_STAT = 707;
    
    static int RQ_DELETE_GAME = 708;
    static int RQ_SAVE_GAME = 709;
    static int RQ_LOAD_GAME = 710;
    
    static int RQ_CLOSE = 999;
}
