/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import com.golden.gamedev.GameEngine;
import com.golden.gamedev.GameObject;
import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.TTextField;
import com.golden.gamedev.gui.toolkit.FrameWork;
import com.golden.gamedev.object.Timer;
import com.golden.gamedev.object.background.ImageBackground;
import java.awt.Graphics2D;
import java.util.ArrayList;
import game.PlayerStatistic;
import sqlrequest.ServerConnector;
import triwizardmaze.themegtge.TriwizardmazeTheme;
import view.gui.GGameScreen;
import view.gui.GProfileScreen;
import view.gui.TImageButton;
import view.gui.IBasicSceneListener;

/**
 *
 * @author godric
 */
public class SProfileScene extends GameObject {

    private final static int BUTTON_WIDTH = 150;
    private final static int BUTTON_HEIGHT = 40;

    private final static int X_STATUS = 200;
    private final static int Y_STATUS = 200;

    private ImageBackground _imgbg;
    private FrameWork _frame;     // main frame

    private GProfileScreen _profileScreen;

    Timer timer;

    public SProfileScene(GameEngine ge) {
        super(ge);
    }

    @Override
    public void initResources() {
        _imgbg = new ImageBackground(bsLoader.getStoredImage("pix/background/menu.png"));

        _profileScreen = new GProfileScreen(
                new IBasicSceneListener() {
            @Override
            public void OnButtonEvent(TButton intiator) {
                OnButtonClicked(intiator);
            }
                }, this.bsLoader);
                        
        _frame = new FrameWork(bsInput, getWidth(), getHeight());
        _frame.installTheme(new TriwizardmazeTheme());
        _frame.setContentPane(_profileScreen);

//        ArrayList<PlayerStatistic> tops = ServerConnector.getTopPlayers();
//        TTextField textField;
//        if (tops == null) {
//            textField = new TTextField("Please check your connection and try again later!",
//                    X_STATUS, Y_STATUS, getWidth()-X_STATUS*2, 30);
//            textField.setEditable(false);
//            _frame.add(textField);
//        } else {
//            int width = (getWidth()-X_STATUS*2)/2;
//            // name
//            textField = new TTextField("Nickname",
//                    X_STATUS, Y_STATUS, width, 30);
//            textField.setEditable(false);
//            _frame.add(textField);
//            // cup
//            textField = new TTextField("Cups",
//                    X_STATUS+width, Y_STATUS, width, 30);
//            textField.setEditable(false);
//            _frame.add(textField);
//            for (int i = tops.size()-1; i >= 0; --i) {
//                // add name
//                textField = new TTextField(tops.get(i).getPlayerName(),
//                    X_STATUS, _frame.get().getY()+35, width, 30);
//                textField.setEditable(false);
//                _frame.add(textField);
        // add cup
//                textField = new TTextField(String.valueOf(tops.get(i).getGobletOwned()),
//                    X_STATUS+width, _frame.get().getY(), width, 30);
//                textField.setEditable(false);
//                _frame.add(textField);
    }

    @Override
    public void update(long l) {
        _frame.update();
    }

    @Override
    public void render(Graphics2D gd) {
        _imgbg.render(gd);
        _frame.render(gd);
    }
    
    private void OnButtonClicked(TButton initiator){
        if (initiator.getText().equals("Back")){
            parent.nextGameID = 1;
            finish();
        }
    }

}
