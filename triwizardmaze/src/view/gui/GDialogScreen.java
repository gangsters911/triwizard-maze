/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.TLabel;
import com.golden.gamedev.gui.toolkit.TContainer;
import game.Triwizardmaze;
import view.gui.TRoleButton.ButtonRole;

/**
 *
 * @author still
 */
public class GDialogScreen extends TContainer {

    private IBasicSceneListener _modelListener;

    private TLabel _dialogText;
    
    private TRoleButton _dialogYesRoleButton;
    
    private TRoleButton _dialogNoRoleButton;
    
    public GDialogScreen(IBasicSceneListener model) {
       
        super(0, 0,
                (int) Triwizardmaze.WINDOW_SIZE.getWidth(),
                (int) Triwizardmaze.WINDOW_SIZE.getHeight());
        _modelListener = model;
        init();
    }

    public void init() {
        initButtons();

        /*Init dialog*/
        _dialogText = new TLabel("DIALOG", 
                DIALOG_X, DIALOG_Y, DIALOG_WIDTH, DIALOG_HEIGTH);
        this.add(_dialogText);
    }

    /**
     * Initializes buttons on maze pause scene
     */
    private void initButtons() {

        _dialogYesRoleButton = new TRoleButton("YES", 131, DIALOG_Y + 360, BUTTON_WIDTH + 20, BUTTON_HEIGHT, ButtonRole.ACCEPT) {
            
            @Override
            public void doAction() {
                
                _modelListener.OnButtonEvent(_dialogYesRoleButton);
            }
        };
        
        this.add(_dialogYesRoleButton);
        
        _dialogNoRoleButton = new TRoleButton("NO", 642, DIALOG_Y + 360, BUTTON_WIDTH + 20, BUTTON_HEIGHT, ButtonRole.REJECT){
            @Override
                public void doAction() {
                    _modelListener.OnButtonEvent(_dialogNoRoleButton);
                }
        };
        this.add(_dialogNoRoleButton);
    }
    
    public void showDialog (String dialogInitiator, String question, String acceptText, String rejectText){
        _dialogText.setText(question);
        
        _dialogYesRoleButton.setText(acceptText.toUpperCase());
        _dialogYesRoleButton.setMentorName(dialogInitiator);
        
        _dialogNoRoleButton.setText(rejectText.toUpperCase());
        _dialogNoRoleButton.setMentorName(dialogInitiator);
    }

    private final static int BUTTON_WIDTH = 250;
    private final static int BUTTON_HEIGHT = 45;
    private final static int DIALOG_X = 112;
    private final static int DIALOG_Y = 150;
    private final static int DIALOG_WIDTH = 800;
    private final static int DIALOG_HEIGTH = 200;
    
    @Override
    public String UIName() {
        return "Faded Screen";
    }
}
