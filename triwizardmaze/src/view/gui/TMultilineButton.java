/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

import com.golden.gamedev.gui.TButton;

/**
 *
 * @author still
 */
public class TMultilineButton extends TButton{
    
    public TMultilineButton(String string, int x, int y, int w, int h) {
        super(string, x, y, w, h);
    }
    
    /**
     * 
     * @return 
     */
    public String UIName() {
        return "Multiline button";
    }
    
}
