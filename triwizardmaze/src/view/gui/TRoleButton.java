/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

import com.golden.gamedev.gui.TButton;

/**
 *
 * @author still
 */
public class TRoleButton extends TButton{
    private ButtonRole _role;
    private String _mentorName; //TODO: Better naming
    
    public TRoleButton(String text, int x, int y, int w, int h, ButtonRole role) {
        super(text, x, y, w, h);
        _role = role;
    }
    
    public ButtonRole getRole(){
        return _role;
    }
    
    public String getMentorName () {
        return _mentorName;
    }
    
    public void setMentorName(String mentorName){
        _mentorName = mentorName;
    }
    public static enum ButtonRole {
        ACCEPT,
        REJECT
    }
    
    @Override
    public void doAction(){
        
    }
}
