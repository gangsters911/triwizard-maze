/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

import com.golden.gamedev.engine.BaseLoader;
import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.TLabel;
import com.golden.gamedev.gui.toolkit.TContainer;
import game.PlayerStatistic;
import game.Triwizardmaze;

/**
 *
 * @author still
 */
public class GProfileScreen extends TContainer{

    IBasicSceneListener _modelListener;
    TAlignedLabel _playerName;
    TAlignedLabel _winsCount;
    TAlignedLabel _totalKills;
    
    public GProfileScreen(IBasicSceneListener model, BaseLoader loader) {
       
        super(0, 0,
                (int) Triwizardmaze.WINDOW_SIZE.getWidth(),
                (int) Triwizardmaze.WINDOW_SIZE.getHeight());
        
        _modelListener = model;
        
        _playerName = new TAlignedLabel("playerName", 640, 330, 250, 50);
        _playerName.alignCenter();
        
        _winsCount = new TAlignedLabel("winsCount", 640, 400, 250, 50);
        _winsCount.alignLeft();
        
        _totalKills = new TAlignedLabel("totalKills", 640, 470, 250, 50);
        _totalKills.alignLeft();
        
        this.add(_playerName);
        this.add(_winsCount);
        this.add(_totalKills);
        
        this.add(new TButton("Back", 720, 700, 100, 35) {
            @Override
            public void doAction() {
                _modelListener.OnButtonEvent(this);
            }
        });
        
        init();
    }

    public void init(){
        PlayerStatistic stat = Triwizardmaze.getPlayerInstance();
        
        _playerName.setText(PROFILE_TEXT + stat.getPlayerName());
        _winsCount.setText(WINS_TEXT + stat.getWonGamesCount());
        _totalKills.setText(KILLS_TEXT + stat.getSummaryKillCount());
        
    }
    @Override
    public String UIName() {
        return "Transparent Screen";
    }
    
    private final static String PROFILE_TEXT = "Hello, ";
    private final static String WINS_TEXT = "Winned games: ";
    private final static String KILLS_TEXT = "Kill count: ";
    
}
