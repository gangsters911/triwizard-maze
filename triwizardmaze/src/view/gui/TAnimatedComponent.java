/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

import com.golden.gamedev.gui.toolkit.TComponent;
import com.golden.gamedev.object.Timer;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author still
 */
public class TAnimatedComponent extends TComponent{
   
    // sprite images
    private transient BufferedImage[] _sprites;
    private int currentFrame;
    private int startFrame;
    private int finishFrame;

    // true, sprite is animated
    private boolean animate;

    // true, the animation is continously (looping)
    private boolean loopAnim = true;

    private Timer animationTimer;

    public TAnimatedComponent(int x, int y, BufferedImage[] sprites, int timer) {
        super(x, y, sprites[0].getWidth(), sprites[0].getHeight());
        startFrame = 0;
        currentFrame = startFrame;
        finishFrame = sprites.length - 1;
        animate = true;
        loopAnim = true;
        animationTimer = new Timer(timer);
        _sprites = sprites;
    }

    @Override
    public void update() {
        super.update();
        
        if (animate && animationTimer.action(1)) {
            updateAnimation();
        }
    }

    @Override
    public void render(Graphics2D g) {
        int width = _sprites[currentFrame].getWidth();
        int height = _sprites[currentFrame].getHeight();
        g.drawImage(_sprites[currentFrame], this.getScreenX(), this.getScreenY(), null); 
    }

    private void updateAnimation() {
        if (++currentFrame > finishFrame) {
            currentFrame = startFrame;

            if (!loopAnim) {
                animate = false;
            }
        }
    }

    @Override
    public String UIName() {
        return "Animated Component";
    }
}
