/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.golden.gamedev.object.Timer;
import java.util.Random;
import maze.AbstrObject;
import navigation.CellPosition;
import view.Game;

/**
 *
 * @author godric
 */
public class GobletController extends TeleportationAI {
    
    Timer timer;
    
    public GobletController(Game game, AbstrObject obj) {
        super(game, obj);
        timer = new Timer(3000);
    }
    
    @Override
    protected CellPosition getPosition(long elapsedTime) {
        if (timer.action(elapsedTime)) {
            // TODO moving goblet
            return _game.getMaze().getCell();
        } 
        return null;
    }
    
}
