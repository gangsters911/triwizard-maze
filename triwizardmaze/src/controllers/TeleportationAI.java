/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import maze.AbstrObject;
import navigation.CellPosition;

/**
 *
 * @author godric
 */
public abstract class TeleportationAI implements StrategyAI{
    protected view.Game _game;
    protected AbstrObject _object;
    
    public TeleportationAI(view.Game game, AbstrObject obj) {
        _game = game;
        _object = obj;
    }
    
    @Override
    public void update(long elapsedTime) {
        _object.setPosition(getPosition(elapsedTime));
    }

    protected abstract CellPosition getPosition(long elapsedTime);
    
}
