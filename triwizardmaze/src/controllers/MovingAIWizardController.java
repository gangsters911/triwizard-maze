/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import maze.Wizard;
import com.golden.gamedev.object.Timer;
import java.util.ArrayList;
import java.util.Random;
import navigation.Direction;
import view.Game;

/**
 *
 * @author godric
 */
public class MovingAIWizardController extends MovingAI {
    

    Timer timer;
    
    public MovingAIWizardController(Game game, Wizard obj) {
        super(game, obj);
        timer = new Timer(1000);
    }
    
    public ArrayList<Direction> getAvailableDirections() {
        ArrayList<Direction> res = new ArrayList<>();
        for (int i = 0; i < MovingAI.directions.length; ++i) {
            if (((Wizard)_character).canMove(MovingAI.directions[i])) {
                res.add(MovingAI.directions[i]);
            }
        }
        return res;
    }
    
    public Direction nextDirection() {
        ArrayList<Direction> ds = getAvailableDirections();
        Direction res = null;
        Random rd = new Random();
        if (!ds.isEmpty()) {
            int index = rd.nextInt(ds.size());
            res = ds.get(index);
        }
        return res;
    }

    @Override
    protected Direction getDirection(long elapsedTime) {
        if (timer.action(elapsedTime)) {
            Random rd = new Random();
            if(rd.nextInt(2193)%16 == 0){
                this._character.setPosition(_game.getMaze().getCell());
            }
            return nextDirection();
        }
        return null;
    }

}