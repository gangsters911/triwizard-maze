/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.golden.gamedev.object.Timer;
import java.util.ArrayList;
import java.util.Random;
import maze.Character;
import maze.Enemy;
import navigation.Direction;
import view.Game;

/**
 *
 * @author thane
 */
public class MovingAIMonsterController extends MovingAI{

     Timer timer;
    
    public MovingAIMonsterController(Game game, Enemy obj) {
        super(game, obj);
        timer = new Timer(1000);
    }
    
    public ArrayList<Direction> getAvailableDirections() {
        ArrayList<Direction> res = new ArrayList<>();
        for (int i = 0; i < MovingAI.directions.length; ++i) {
            if (((Enemy)_character).canMove(MovingAI.directions[i])) {
                res.add(MovingAI.directions[i]);
            }
        }
        return res;
    }
    
    public Direction nextDirection() {
        ArrayList<Direction> ds = getAvailableDirections();
        Direction res = null;
        Random rd = new Random();
        if (!ds.isEmpty()) {
            int index = rd.nextInt(ds.size());
            res = ds.get(index);
        }
        return res;
    }

    @Override
    protected Direction getDirection(long elapsedTime) {
        if (timer.action(elapsedTime)) {
            return nextDirection();
        }
        return null;
    }
    
}
