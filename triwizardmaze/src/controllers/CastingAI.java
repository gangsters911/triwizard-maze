/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import maze.Wizard;
import view.Game;

/**
 *
 * @author godric
 */
public abstract class CastingAI implements StrategyAI {
    
    protected view.Game _game;
    protected Wizard _wizard;

    public CastingAI(Game _game, Wizard _wizard) {
        this._game = _game;
        this._wizard = _wizard;
    }
    
    @Override
    public void update(long l) {
        Class s = getSpell(l);
        if (s != null) {
            _wizard.makeSpell(s);
        }
    }
    
    protected abstract Class getSpell(long l);
    
}
