/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triwizardmaze.themegtge;

import com.golden.gamedev.gui.toolkit.GraphicsUtil;
import com.golden.gamedev.gui.toolkit.TComponent;
import com.golden.gamedev.gui.toolkit.UIRenderer;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Transparency;
import java.awt.image.BufferedImage;

/** Simple custom renderer for game screen in maze
 *
 * @author Alexander Lyashenko
 */
public class TMGameUiRenderer extends UIRenderer{
    
    public TMGameUiRenderer() {
            this.put("Background Color", new Color(255, 255, 255, 0));
	}
	
    /**
     * Returns UI Factory name
     * @return 
     */
    @Override
	public String UIName() {
		return "MazeUI";
	}
	
    /**
     * Retruns UI Factory description (probably useless)
     * @return 
     */
    @Override
	public String[] UIDescription() {
		return new String[] {
		        "Panel", "Panel Disabled"
		};
	}

    /**
     * Creates and draw UI components
     * @param component component for drawing
     * @param w width of component
     * @param h height of component
     * @return drawed frames as Buffered image array
     */
    @Override
    public BufferedImage[] createUI(TComponent component, int w, int h) {
		BufferedImage[] ui = GraphicsUtil.createImage(1, w, h,
		        Transparency.TRANSLUCENT);
		
		String[] color = new String[] {
		        "Background Color"
		};
		
		for (int i = 0; i < ui.length; i++) {
			Graphics2D g = ui[i].createGraphics();
			
			g.setColor((Color) this.get(color[i], component));
			g.fill3DRect(0, 0, w, h, true);
                        g.dispose();
		}
		
		return ui;
	}
    
    /**
     * Overloaded method for processing interactive parts of drawing component
     * @param component component for drawing
     * @param ui drawed frames
     */
    @Override
    public void processUI(TComponent component, BufferedImage[] ui) {
	}
	
    /**
     * Rendering frames into the screen
     * @param g screen
     * @param x component of point, from where we paint frame
     * @param y component of point, from where we paint frame
     * @param component component for drawing
     * @param ui drawed frames
     */
    @Override
	public void renderUI(Graphics2D g, int x, int y, TComponent component, BufferedImage[] ui) {
            g.drawImage(ui[0], x, y, null);
	}
}
