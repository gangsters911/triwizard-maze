/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triwizardmaze.themegtge;

import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.theme.basic.BButtonRenderer;
import com.golden.gamedev.gui.toolkit.GraphicsUtil;
import com.golden.gamedev.gui.toolkit.TComponent;
import com.golden.gamedev.gui.toolkit.UIConstants;
import com.golden.gamedev.object.GameFont;
import com.golden.gamedev.object.GameFontManager;
import com.golden.gamedev.util.FontUtil;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author godric, Alexander Lyashenko
 */
class TMButtonRenderer extends BButtonRenderer {

    public TMButtonRenderer() {
        GameFontManager _fontManager = new GameFontManager();
        URL _fontPath = null;

        try {
            _fontPath = new File("resources/fonts/magic.ttf").toURI().toURL();
        } catch (MalformedURLException ex) {
            Logger.getLogger(TMButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }

        GameFont typicalFont = _fontManager.getFont(
                FontUtil.createTrueTypeFont(_fontPath, Font.PLAIN, 56));

        GameFont pressedFont = _fontManager.getFont(
                FontUtil.createTrueTypeFont(_fontPath, Font.PLAIN, 48));

        this.put("Text Font", typicalFont);
        this.put("Text Pressed Font", pressedFont);

        put("Text Color", new Color(210, 105, 30));         //#D2691E
        put("Text Over Color", new Color(255, 215, 0));    //#D2691E            
        put("Text Pressed Color", new Color(255, 215, 0));   //#8B4513
        put("Text Disabled Color", new Color(0,0,0,0));  //#FFA500

        this.put("Text Horizontal Alignment Integer", UIConstants.CENTER);
        this.put("Text Vertical Alignment Integer", UIConstants.CENTER);
        this.put("Text Insets", new Insets(0, -100, 0, 0));
        this.put("Text Vertical Space Integer", new Integer(-2));
//        
    }

    @Override
    public void processUI(TComponent component, BufferedImage[] ui) {
        TButton button = (TButton) component;

        String[] font = new String[]{
            "Text Font", "Text Font", "Text Pressed Font",
            "Text Font"
        };

        String[] color = new String[]{"Text Color",
            "Text Over Color", "Text Pressed Color", "Text Disabled Color"};

        String[] document = GraphicsUtil.parseString(button.getText());
        for (int i = 0; i < 4; i++) {
            Graphics2D g = ui[i].createGraphics();
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            GraphicsUtil.drawString(
                    g,
                    document,
                    button.getWidth(),
                    button.getHeight(),
                    (GameFont) this.get(font[i], component),
                    (Color) this.get(color[i], component),
                    (Integer) this.get(
                            "Text Horizontal Alignment Integer", component),
                    (Integer) this.get(
                            "Text Vertical Alignment Integer", component),
                    null,
                    (Integer) this.get(
                            "Text Vertical Space Integer", component));
            g.dispose();
        }
    }

    @Override
    public BufferedImage[] createUI(TComponent component, int w, int h) {
        BufferedImage[] ui = GraphicsUtil.createImage(4, w, h, Transparency.BITMASK);
        return ui;
    }

    
}
