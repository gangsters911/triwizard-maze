/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triwizardmaze.themegtge;

import com.golden.gamedev.gui.theme.basic.BLabelRenderer;
import com.golden.gamedev.gui.toolkit.UIConstants;
import com.golden.gamedev.object.GameFont;
import com.golden.gamedev.object.GameFontManager;
import com.golden.gamedev.util.FontUtil;
import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author still
 */
public class TMLabelRenderer extends BLabelRenderer {

    private String _fabricName;

    public TMLabelRenderer(String Align) {
        super();

        GameFontManager _fontManager = new GameFontManager();
        URL _fontPath = null;

        try {
            _fontPath = new File("resources/fonts/magic.ttf").toURI().toURL();
        } catch (MalformedURLException ex) {
            Logger.getLogger(TMButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }

        GameFont typicalFont = _fontManager.getFont(
                FontUtil.createTrueTypeFont(_fontPath, Font.PLAIN, 50));

        this.put("Text Color", new Color(210, 105, 30));
        this.put("Text Disabled Color", new Color(0, 0, 0, 0));

        this.put("Text Font", typicalFont);
        this.put("Text Disabled Font", typicalFont);

        if (Align == null){
            this.put("Text Horizontal Alignment Integer", UIConstants.CENTER);
            _fabricName = "Label";
            return;
        }
        switch (Align) {
                case "LA Label":
                    this.put("Text Horizontal Alignment Integer", UIConstants.LEFT);
                    break;

                case "RA Label":
                    this.put("Text Horizontal Alignment Integer", UIConstants.RIGHT);
                    break;

                case "CA Label":
                    this.put("Text Horizontal Alignment Integer", UIConstants.CENTER);
                    break;

                default:
                    this.put("Text Horizontal Alignment Integer", UIConstants.CENTER);
                    Align = "Label";
                    break;

            }

            _fabricName = Align;
    }
    
    @Override
	public String UIName() {            
            return _fabricName;
        }
}
