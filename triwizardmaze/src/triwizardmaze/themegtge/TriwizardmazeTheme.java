/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triwizardmaze.themegtge;

import com.golden.gamedev.gui.theme.basic.BasicTheme;
import java.awt.Color;

/**
 *
 * @author godric
 */
public class TriwizardmazeTheme extends BasicTheme {

    public TriwizardmazeTheme() {
        installUI(new TMButtonRenderer());
        installUI(new TMMultilineButtonRenderer());
        installUI(new TMGameUiRenderer());
        installUI(new TMColorizedScreenRenderer("Faded Screen", new Color(0,0,0,100)));
        installUI(new TMColorizedScreenRenderer("Transparent Screen", new Color(0,0,0,0)));
        installUI(new TMAnimatedRenderer());
        installUI(new TMLabelRenderer("LA Label"));
        installUI(new TMLabelRenderer("RA Label"));
        installUI(new TMLabelRenderer("CA Label"));
        installUI(new TMLabelRenderer(null));
        installUI(new TMSelectorButtonRenderer());
        installUI(new TMImageButtonRenderer());

    }

    public String getName() {
        return "Triwizardmaze Theme";
    }

}
