/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import navigation.Direction;

/**
 *
 * @author godric
 */
public interface ICharacter {
    public void move(Direction d);
    public void die();
}
