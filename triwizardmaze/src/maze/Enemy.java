/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import navigation.Direction;

/**
 *
 * @author godric
 */
public abstract class Enemy extends Character {

    public Enemy() {
        super(null);
        _mana = 0;
        _health = 50;
        _protection = 0;
        _direction = Direction.north();
        _paused = false;
    }
    
    public Enemy(World world) {
        super(world);
        _mana = 0;
        _health = 50;
        _protection = 0;
        _direction = Direction.north();
        _paused = false;
    }

    @Override
    public void setupRecoveringMana(){
        
    }
    
    @Override
    public boolean isProtected() {
        return false;
    }
    
    @Override
    abstract public void die() ;
    
    abstract public void attack(Wizard w);
    

}
