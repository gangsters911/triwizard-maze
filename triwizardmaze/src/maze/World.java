/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import java.util.ArrayList;
import com.golden.gamedev.object.Background;
import com.golden.gamedev.object.PlayField;
import com.golden.gamedev.object.SpriteGroup;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import magic.Magic;
/**
 *
 * @author godric
 */
public class World<Position> extends PlayField {
    
    private HashMap<Class, List<AbstrObject>> _objects;

    public World(Background b) {
        super(b);
        myconstr();
    }

    public World() {
        super();
        myconstr();
    }
    
    private void myconstr() {
        this._objects = new HashMap<Class, List<AbstrObject> >();
        this.addGroup(new SpriteGroup("Walls"));
        this.addGroup(new SpriteGroup("Magics"));
    }
    
    public void clear() {
        this._objects.clear();
    }
    
    public boolean addObject(Position pos, AbstrObject obj) {
        boolean res = false;
        Class objClass = obj.getClass();
        if (obj.setPosition(pos)) {
            if( _objects.containsKey(objClass))  {     	
                _objects.get(objClass).add(obj);
            } else {                                    
                List<AbstrObject> objList = new ArrayList<AbstrObject>();
                objList.add(obj);
                _objects.put(objClass, objList);
            }
            res = true;
        }
        obj.setBackground(this.getBackground());
        if (obj instanceof Wall) {
            this.getGroup("Walls").add(obj);
        } else if (obj instanceof Magic) {
            this.getGroup("Magics").add(obj);
        }
        
        return false;
    }

    public void clearClass(Class objType) {
        if (_objects.containsKey(objType)) {
            _objects.get(objType).clear();
        }
        if (objType == Wall.class) {
            this.getGroup("Walls").clear();
        } else if (objType == Magic.class) {
            this.getGroup("Magics").clear();
        }
    }
    
    public boolean removeObject(AbstrObject obj){
        boolean success = false;
        Class objClass = obj.getClass();
        obj.setActive(false);
        if (_objects.containsKey(objClass)) {
            success = _objects.get(objClass).remove(obj);
            if(success) 
                obj.setPosition(null);
        }
        return success;
    }
	
    public List<AbstrObject> objects(Class objType) {
        List<AbstrObject> objList = new ArrayList<AbstrObject>();
        if(_objects.containsKey(objType)) { 
            objList.addAll( _objects.get(objType) );
        }
        return objList;
    }

    public List<AbstrObject> objects() {
        List<AbstrObject> objList = new ArrayList<AbstrObject>();
        for (Map.Entry<Class, List<AbstrObject> > entry : _objects.entrySet()) { 
            objList.addAll( entry.getValue() );
        }
        return objList;
    }
    
    public List<AbstrObject> objects(Position pos) {
	List<AbstrObject> objList = new ArrayList<AbstrObject>();
        for (Map.Entry<Class, List<AbstrObject> > entry : _objects.entrySet()) { 
            for (AbstrObject obj  : entry.getValue()) {  
                if(obj.position().equals(pos)) {
                    objList.add( obj );
                }
            }
        }
        return objList;    
    }

    public List<AbstrObject> objects(Class objType, Position pos) {
        List<AbstrObject> objList = new ArrayList<AbstrObject>();
        if (_objects.containsKey(objType)) { 
           for (AbstrObject obj  : _objects.get(objType)) {  
                if (obj.position().equals(pos)) {
                    objList.add( obj );
                }
            }
        }
        return objList;
    }
    
}
