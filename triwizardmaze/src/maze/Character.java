/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import controllers.AIMath;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import magic.AttackingMagic;
import magic.Expelliarmus;
import navigation.CellPosition;
import navigation.Direction;
import navigation.MiddlePosition;

/**
 *
 * @author godric
 */
public abstract class Character extends AbstrObject<CellPosition> implements ICharacter {
    
    protected int _health;
    protected int _mana;
    protected int _protection;
    protected ArrayList<Class> magics;
    protected Direction _direction;
    protected Timer _recoverManaTimer;
    protected boolean _paused;
    
    public Character(World world) {
        super(world);
        _mana = 100;
        _health = 100;
        _protection = 0;
        magics = new ArrayList<>();
        _direction = Direction.south();
        _paused = false;
        setupRecoveringMana();
        
    }
    
    public void addMagic(Class mg) {
        if (!magics.contains(mg)) {
            magics.add(mg);
        }
    }

    //--------------------------------------------------------------------------
    // Methods for health.
    public void injure(int m) {
        this._health -= (m * (100 - _protection) / 100);
        if (this._health <= 0) {
            die();
        }
    }
    
    public void hadBeenHit(AttackingMagic spell){
        
        if((Wizard)this != spell.getMaker()){
            this.injure(spell.getDamage());
        }
    }
    
    public void cure(int m) {
        if (m > 0) {
            _health += m;
            if (_health > 100) {
                _health = 100;
            }
        }
    }
    
    public void setHealth(int _health) {
        if (0 <= _health && _health <= 100) {
            this._health = _health;
        }
    }
    
    public int getHealth() {
        return _health;
    }

    //--------------------------------------------------------------------------
    // Methods for mana.
    public void setMana(int _mana) {
        if (0 <= _mana && _mana <= 100) {
            this._mana = _mana;
        }
    }
    
    public int getMana() {
        return _mana;
    }
    
    public void recoverMana(int value) {
        if (0 < value) {
            _mana += value;
            if (_mana > 100) {
                _mana = 100;
            }
        }
    }
    
    /**
     * Setup recover 10 mana - each 1s 
     */
    protected void setupRecoveringMana() {
        this._recoverManaTimer = new Timer();
        this._recoverManaTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(!_paused){
                    recoverMana(10);
                }
            }
        }, 0, 1000);
    }

    //--------------------------------------------------------------------------
    // Methods for direction.
    public Direction getDirection() {
        return _direction;
    }
    
    public void setDirection(Direction _direction) {
        this._direction = _direction;
    }
    
    public boolean canMove(Direction d) {
        
        return _position.hasNext(d)
                && _world.objects(Wall.class, new MiddlePosition(_position, d)).isEmpty();
    }
    
    public void setProtection(int _protection) {
        if (0 <= _protection && _protection <= 100) {
            this._protection = _protection;
        }
    }
    
    public boolean isProtected() {
        return this._protection > 0;
    }
    
    @Override
    public void move(Direction d) {
        if (d != null) {
            if (canMove(d)) {
                //this.setImage(ImageUtil.rotate(this.getImage(), _direction.clockwise_angle(d)));
                this.setPosition(_position.next(d));
                //_direction = d;
            }
            
            _direction = d;
        }
    }
    
    @Override
    public void die() {
        _world.removeObject(this);
    }
    
    @Override
    public String toString() {
        return super.toString() + ' '
                + _direction.toString() + ' '
                + _health + ' ' + _mana;
    }

    //--------------------------------------------------------------------------
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        super.readExternal(in);
        _direction = AIMath.getDirectionOfAngle(in.readInt());
        _health = in.readInt();
        _mana = in.readInt();        
    }
    
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        super.writeExternal(out);
        out.writeInt(_direction.get_angle());
        out.writeInt(_health);
        out.writeInt(_mana);
    }
    
    public boolean isAlive(){
        return _health > 0;
    }
}
