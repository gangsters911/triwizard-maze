/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 *
 * @author thane
 */
public class FakeGoblet extends Item{
    
    
    private int _id;
    
    public FakeGoblet() {
        super(null);
    }

    public FakeGoblet(World world) {
        super(world);
    }
    
    public FakeGoblet(World world, int id) {
        super(world);
        _id = id;
    }
    
    @Override
    public String toString() {
        return _position.toString() + ' ' + _id;
    }
    
    
    @Override
    public void take(maze.Character o){
        _world.removeObject(this);
    }
    
    
    //--------------------------------------------------------------------------
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        super.readExternal(in);
        _id = in.readInt();
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        super.writeExternal(out);
        out.writeInt(_id);
    }
}
