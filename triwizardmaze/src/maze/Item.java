/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import navigation.CellPosition;

/**
 *
 * @author godric
 */
public abstract class Item extends AbstrObject<CellPosition>{
    maze.Character owner;

    public Item(World world) {
        super(world);
        owner = null;
    }
    
    public void take(maze.Character o) {
        owner = o;
    }
    
    public boolean hasTake() {
        return owner != null;
    }
    
    public maze.Character owner() {
        return this.owner;
    }
    
}
