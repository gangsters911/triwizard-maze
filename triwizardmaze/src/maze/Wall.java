/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import navigation.MiddlePosition;

/**
 *
 * @author godric
 */
public class Wall extends AbstrObject<MiddlePosition> {

    private int _rigidity;

    public Wall() {
        super(null);
        _rigidity = 1;
    }

    public Wall(World w) {
        super(w);
        _rigidity = 1;
    }

    public Wall(World w, int rigidity) {
        super(w);
        this._rigidity = rigidity;
    }

    public void setRigidity(int r) {
        _rigidity = r;
    }

    public int getRigidity() {
        return _rigidity;
    }
}
