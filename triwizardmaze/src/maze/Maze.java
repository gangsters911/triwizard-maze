/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import collisions.AttackMagicHitCharacterCollision;
import collisions.MagicHitWall;
import collisions.MagicOutCollision;
import collisions.MonsterHitWizardCollision;
import collisions.TakeGobletCollision;
import collisions.WizardFoundFakeGoblet;
import collisions.WizardHitTrapCollision;
import com.golden.gamedev.object.SpriteGroup;
import controllers.StrategyAI;
import game.GameSetting;
import game.Triwizardmaze;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import navigation.CellPosition;
import navigation.MiddlePosition;
import generator.GenMap;
import view.Game;
import view.gui.IMazeSceneListener;

/**
 * The main calculation class.
 * @author godric
 */
public class Maze {

    /**
     * *********************************************************************
     */
    /**
     * ****************************Attributes*******************************
     */
    /**
     * *******************************************************************
     */
    private int _id = 0;
    private int _width = GameSetting.WIDTH;
    private int _height = GameSetting.HEIGHT;
    private World _world = new World(GameSetting._imgbg);
    private IMazeSceneListener _view;
    
    public Goblet _goblet;
    public Wizard _hero;
    public ArrayList<StrategyAI> controllers = new ArrayList<>();
    
    /**
     * Dispersion of static walls in the maze
     */
    private int dispersion;
    
    /**
     * List of coordinates of static walls
     */
    public ArrayList<MiddlePosition> staticWalls = new ArrayList<>();

    private final Class[] saveClasses = {Wall.class, Wizard.class, Trap.class, Goblet.class, FakeGoblet.class, SimpleMonster.class, InvincibleMonster.class};

    private Timer _changeWallsTimer;
    private boolean _isPaused;

    /**
     * *********************************************************************
     */
    /**
     * *****************************Methods********************************
     */
    /**
     * *******************************************************************
     */
    /*------------------------------------------------------------------------*/
 /*------------------------------Constructors-----------------------------*/
 /*----------------------------------------------------------------------*/
    public Maze(GameSetting.Heroes hero, IMazeSceneListener view) {

        this._view = view;
        createWizards(hero);
        createEnemies();
        createTraps();
        createItems();

        // setup timer to change wall
        this._changeWallsTimer = new Timer();
        this._changeWallsTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!_isPaused)
                generateNewWalls();
            }
        }, 0, 5000);
    }

    public Maze(Game gm, IMazeSceneListener view) throws IOException, ClassNotFoundException {

        this._view = view;
        loadGame();
        
        // setup timer to change wall
        this._changeWallsTimer = new Timer();
        this._changeWallsTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!_isPaused)
                generateNewWalls();
            }
        }, 0, 5000);
    }

    /*--------------------------------------------------------------------------*/
 /*------------------------------Section create-----------------------------*/
 /*------------------------------------------------------------------------*/
    /**
     *
     */
    private void createEnemies() {
        for (int i = 0; i < GameSetting.INVINCIBLE_ENEMY_COUNT; ++i) {

            _world.addObject(getCell(), new InvincibleMonster(_world));
        }
        for (int i = 0; i < GameSetting.SIMPLE_ENEMY_COUNT; ++i) {

            _world.addObject(getCell(), new SimpleMonster(_world));
        }
    }

        
    /**
     *
     */
    private void createItems() {
        // --- add goblets
        _world.addObject(getCell(), new Goblet(_world));
        createFakeGoblets();
    }

    /**
     *
     */
    private void createTraps() {
        Random gen = new Random();
        for (int i = 0; i < GameSetting.TRAP_COUNT; ++i) {
            _world.addObject(getCell(), new Trap(_world, gen.nextInt(10) + 1));
        }
    }

    private void createFakeGoblets() {

        for (int i = 0; i < GameSetting.FAKE_GOBLET_COUNT; ++i) {

            addFakeGoblet(getCell());
        }
    }

    public CellPosition getCell() {
        Random rd = new Random();
        CellPosition cell = null;
        boolean flag = true;
        do {
            CellPosition test = new CellPosition(
                    rd.nextInt(game.GameSetting.HEIGHT) + 1,
                    rd.nextInt(game.GameSetting.WIDTH) + 1);
            flag = !_world.objects(test).isEmpty();
            if (!flag) {
                cell = test;
            }
        } while (flag);
        return cell;
    }

    /**
     *
     * @param plHero the value of plHero
     */
    private void createWizards(GameSetting.Heroes plHero) {
        int heroIndex = 0;
        int[] startX = {1, GameSetting.HEIGHT, 1, GameSetting.HEIGHT};
        int[] startY = {1, 1, GameSetting.WIDTH, GameSetting.WIDTH};

        for (GameSetting.Heroes hero : GameSetting.Heroes.values()) {
            addWizard(hero, startX[heroIndex], startY[heroIndex++], hero.equals(plHero));
        }

        List<AbstrObject> tmp = _world.objects(Wizard.class);
        tmp.stream().filter((AbstrObject t) -> (((Wizard) t).getName() == plHero)).forEach((AbstrObject t) -> {
            //_hero = (Wizard) t;
        });
    }

    public boolean isHaveWall(MiddlePosition pos) {
        List<AbstrObject> walls = _world.objects(Wall.class);
        return walls.stream().anyMatch((w) -> (w.position() == pos));
    }

    /**
     *
     * @param hero the value of hero
     * @param x the value of x
     * @param y the value of y
     * @param isPlayer
     */
    private void addWizard(GameSetting.Heroes hero, int x, int y, boolean isPlayer) {

        Wizard newWizard = new Wizard(hero, _world);

        _world.addObject(new CellPosition(x, y), newWizard);

        if (isPlayer) {
            _hero = newWizard;
        }
    }

    /**
     *
     * @param hero the value of hero
     * @param x the value of x
     * @param y the value of y
     */
    private void addFakeGoblet(int x, int y) {
        _world.addObject(new CellPosition(x, y), new FakeGoblet(_world, _id));
        ++_id;
    }

    private void addFakeGoblet(CellPosition cell) {
        _world.addObject(cell, new FakeGoblet(_world, _id));
        ++_id;
    }

    public void setupCollisions() {
        _world.addCollisionGroup(getGroup(Wizard.class), getGroup(Goblet.class), new TakeGobletCollision());
        _world.addCollisionGroup(getGroup(Wizard.class), getGroup(FakeGoblet.class), new WizardFoundFakeGoblet());
        _world.addCollisionGroup(getGroup(Wizard.class), getGroup(Trap.class), new WizardHitTrapCollision());
        _world.addCollisionGroup(_world.getGroup("Magics"), getGroup(Wizard.class), new AttackMagicHitCharacterCollision());
        _world.addCollisionGroup(_world.getGroup("Magics"), _world.getGroup("Walls"), new MagicHitWall());
        _world.addCollisionGroup(_world.getGroup("Magics"), null,
                new MagicOutCollision(GameSetting.MAZE_X_LEFT, GameSetting.MAZE_Y_TOP, GameSetting.MAZE_WIDTH, GameSetting.MAZE_HEIGHT));
        _world.addCollisionGroup(getGroup(SimpleMonster.class), getGroup(Wizard.class), new MonsterHitWizardCollision());
        _world.addCollisionGroup(getGroup(InvincibleMonster.class), getGroup(Wizard.class), new MonsterHitWizardCollision());
        _world.addCollisionGroup(_world.getGroup("Magics"), getGroup(SimpleMonster.class), new AttackMagicHitCharacterCollision());
        _world.addCollisionGroup(_world.getGroup("Magics"), getGroup(InvincibleMonster.class), new AttackMagicHitCharacterCollision());
    }

    /*--------------------------------------------------------------------------*/
 /*------------------------------Section update-----------------------------*/
 /*------------------------------------------------------------------------*/
    public void update (long elapsedTime){
        if (!_isPaused){
            _world.update(elapsedTime);
            
            controllers.stream().forEach((c) -> {
                c.update(elapsedTime);
            });            
            
        checkIfOver();
        }        
    }
    /**
     *
     * @return the int
     */
    private void checkIfOver() {
        if (!_hero.isActive()){
            _isPaused = true;
            _view.gameEnded(EndReason.PLAYER_DEAD);
        } else if (_hero.getGiveUp()) {
            _isPaused = true;
            _view.gameEnded(EndReason.PLAYER_SURREND);
            
        } else {
            Goblet gb = (Goblet) _world.objects(Goblet.class).get(0);
            if (gb.hasTake()){
                _isPaused = true;
                
                if (_hero._hero.equals(((Wizard)gb.owner).getName())){
                    Triwizardmaze.getPlayerInstance().addHighscore(Triwizardmaze.getPlayerInstance().lastHighscore);
                    Triwizardmaze.getPlayerInstance().addWonGame(_hero.getName().value);
                     Triwizardmaze.getPlayerInstance().lastHighscore = 0;
                }
                
                switch (((Wizard)gb.owner).getName()){
                    case HARRY:
                        _view.gameEnded(EndReason.POTTER_WIN);
                        break;
                    case CEDRIC:
                        _view.gameEnded(EndReason.DIGGORY_WIN);
                        break;
                    case VICTOR:
                        _view.gameEnded(EndReason.KRUM_WIN);
                        break;
                    case FLEUR:
                        _view.gameEnded(EndReason.DELACOUR_WIN);
                        break;
                    default:
                        throw new AssertionError(((Wizard)gb.owner).getName().name());
            }            
        }
    }
    }

    /*------------------------------------------------------------------------*/
 /*------------------------------Section load-----------------------------*/
 /*----------------------------------------------------------------------*/
    public void load(Class type, ObjectInputStream oi) throws IOException, ClassNotFoundException{

        int size = 0;
        AbstrObject tmp = null;

        size = oi.readInt();
            for (int i = 0; i < size; ++i) {
                tmp = (AbstrObject) oi.readObject();
                tmp.setWorld(_world);
                _world.addObject(tmp.position(), tmp);
            }

    }

    public void loadGame() throws IOException, ClassNotFoundException{

        ObjectInputStream oi = new ObjectInputStream(new FileInputStream("gamesave.cer"));
           _world.clear();
            int hero = oi.readInt();

            // Load each class.
            for (Class cl : this.saveClasses) {
                load(cl, oi);
            }

            // Setup main hero.
            List<AbstrObject> heroes = _world.objects(Wizard.class);
            for (AbstrObject hr : heroes) {
                if (((Wizard) hr).getName().value == hero) {
                    this._hero = (Wizard) hr;
                    break;
                }
            }
    }

    /*------------------------------------------------------------------------*/
 /*------------------------------Section save-----------------------------*/
 /*----------------------------------------------------------------------*/
    private boolean save(Class type, ObjectOutput oo) {

        List<AbstrObject> tmps = _world.objects(type);
        try {
            oo.writeInt(tmps.size());
            for (AbstrObject tmp : tmps) {
                oo.writeObject(tmp);
            }
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;

    }

    public boolean saveGame() {

        try (ObjectOutputStream oo = new ObjectOutputStream(new FileOutputStream("gamesave.cer"))) {

            oo.writeInt(this._hero.getName().value);
            for (Class cl : this.saveClasses) {
                if (!save(cl, oo)) {
                    return false;
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(Maze.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (Exception ex) {
            Logger.getLogger(Maze.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;

    }

    /*-------------------------------------------------------------------*/
 /*------------------------------Getters-----------------------------*/
 /*-----------------------------------------------------------------*/
    /**
     *
     * @return world object
     */
    public World getWorld() {
        return _world;
    }

    /**
     *
     * @param objType - the type of objects which must be collected
     * @return group of sprites for needed class
     */
    public SpriteGroup getGroup(Class objType) {
        SpriteGroup res = new SpriteGroup("");
        List<AbstrObject> tmp = _world.objects(objType);
        tmp.stream().forEach((AbstrObject obj) -> {
            res.add(obj);
        });
        return res;
    }

    /**
     *
     * @return hero
     */
    public Wizard getHero() {
        return this._hero;
    }

    /**
     *
     * @return array of current walls positions
     */
    public List<Wall> getWalls() {
        return this._world.objects(Wall.class);
    }

    public void setPause (boolean value){
        _isPaused = value;
    }
    
    public boolean isPaused (){
        return _isPaused;
    }
    public static enum EndReason {
        PLAYER_DEAD,
        PLAYER_SURREND,
        POTTER_WIN,
        KRUM_WIN,
        DIGGORY_WIN,
        DELACOUR_WIN
    }

    /***************************************************************************
     * Generator wall
     **************************************************************************/
    
    /**
     * Calculation of the dispersion of static walls in the maze.
     * @param list list of walls
     * @return dispersion of static walls in the maze
     */
    private int searchDispersion (ArrayList<Wall> list) {
        return list.size()/GameSetting.STATIC_WALLS;
    }
    
    /**
     * Rearrange the walls of the maze.
     * @param list list of walls
     * @return modified list of walls
     */
    private ArrayList <Wall> replaceWalls (ArrayList<Wall> list) {
        ArrayList<Wall> walls = new ArrayList<Wall>();

        for (int i = 0, j = 0; i < list.size(); i++) { // For every created wall
            if (i % dispersion == 0) {    // Replace every wall in the place
                Wall w = new Wall();                // Create wall
                w.setPosition(staticWalls.get(j));  // This is replacing
                walls.add(w);                       // Add this wall
                j++;
            } else {
                walls.add(list.get(i));  // Copy as is
            }
        }

        return walls;
    }
    
    /**
     * Filling the maze with walls.
     * @param list list of walls
     */
    private void fillMap (ArrayList<Wall> list) {
        for (Wall re : list) {
            _world.addObject(re.position(), re);
        }
    }
    
    /**
     * Generation of walls in the maze.
     */
    private void generateNewWalls() {
        // Create walls
        ArrayList<Wall> res;
        ArrayList<Wall> walls = new ArrayList<Wall> ();
        _world.clearClass(Wall.class);
        res = GenMap.run();
        
        // If no static walls on the map then make it
        if (GameSetting.STATIC_WALLS > 0 && GameSetting.STATIC_WALLS < 267) {
            if (staticWalls.isEmpty()) {
                dispersion = searchDispersion(res);        // Calculate dispersion of statis walls
                for (int i = 0; i < res.size(); i += dispersion) {
                    staticWalls.add(res.get(i).position()); // Then save its coordinates
                }
            } else {   // If positions of static walls were saved
                walls = replaceWalls(res);
            }
        }
        
        if (walls.isEmpty()) {
            fillMap (res);
        } else {
            fillMap (walls);
        }
    }
}
