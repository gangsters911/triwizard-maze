/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;
import maze.Wizard;

/**
 *
 * @author godric
 */
public class UserProfile {
    
    private String nickname;
    private int score;
    private int enemyKilled;
    private int gobletOwned;
    private int difficultyLevel;
    private String saveGame;

    private Wizard hero;
    
    public UserProfile(String nick) {
        this(nick, 0, 0, 0, 0);
    }
    
    public UserProfile(String nick, int score, int enemy, int goblet, int level) {
        this.nickname = nick;
        this.score = score;
        this.enemyKilled = enemy;
        this.gobletOwned = goblet;
        this.difficultyLevel = level;
        this.hero = null;
        saveGame = "";
    }
    
    public void setHero(Wizard hr) {
        this.hero = hr;
    }
    
    public Wizard getHero() {
        return this.hero;
    }
    
    public String getNickname() {
        return this.nickname;
    }
    
    public int getScore() {
        return this.score;
    }
    
    public int getEnemyKilled() {
        return this.enemyKilled;
    }
    
    public int getGobletOwned() {
        return this.gobletOwned;
    }
    
    public void increaseScore(int value) {
        this.score += value;
    }
    
    public void increaseGoblet() {
        this.gobletOwned += 1;
    }

    public void setSaveGame(String _saveGame) {
        this.saveGame = _saveGame;
    }

    public String getSaveGame() {
        return saveGame;
    }
    
}
