/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magic;

import maze.Wizard;
import maze.World;

/**
 *
 * @author thane
 */
public abstract class MedicineMagic extends Magic {

    protected int _healing;
    
    public MedicineMagic(World world, Wizard mk, int healing) {
        super(world, mk);
        _healing = healing;
    }
    
}