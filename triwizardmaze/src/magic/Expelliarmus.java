/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magic;

import com.golden.gamedev.util.ImageUtil;
import maze.Wizard;
import maze.World;
import navigation.Direction;

/**
 *
 * @author thane
 */
public class Expelliarmus  extends AttackingMagic{
    
    private static String IMG_PATH = "resources/pix/magic/expilliarmus.png";

    public Expelliarmus(World world, Direction d, Wizard mk) {
        super(world, d, mk, 0);
        setImage(IMG_PATH);
        this._cost = 40;
}

    @Override
    public boolean castASpell() {
        if(super.isCapable()){
            this.setImage(ImageUtil.rotate(this.getImage(), Direction.north().clockwise_angle(_direct)));
            this.setMovement(0.2, (450-_direct.get_angle())%360);
            return true;
        }
        return false;
    }
    
}
