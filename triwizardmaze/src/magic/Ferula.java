    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magic;

import com.golden.gamedev.object.Timer;
import maze.Wizard;
import maze.World;

/**
 *
 * @author godric
 */
public final class Ferula extends MedicineMagic{

    private static String IMG_PATH = "resources/pix/magic/ferula.png";
    private Timer timer;
    
    public Ferula(World world, Wizard mk) {
        super(world, mk, 10);
        setImage(IMG_PATH);
        timer = new Timer(500);
        this._cost = 20;
    }
    
    @Override
    public void update(long l) {
        super.update(l);
        this.setPosition(this._maker.position());
        if (timer.action(l)) {
            this._world.removeObject(this);
        }
    }
    
    @Override
    public boolean castASpell() {
        if(super.isCapable()) {
            this._maker.cure(_healing);
            this.setActive(false);
            return true;
        }
        return false;
    }
}
