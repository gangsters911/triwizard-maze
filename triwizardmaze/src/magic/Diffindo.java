/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magic;

import maze.Wizard;
import maze.World;
import navigation.Direction;

/**
 *
 * @author godric
 */
public final class Diffindo extends AttackingMagic{
    
    private static String IMG_PATH = "resources/pix/magic/diffindo.png";
    
    public Diffindo(World world, Direction d, Wizard mk, int dam) {
        super(world, d, mk, dam);
        setImage(IMG_PATH);
        this._cost = 40;
    }
    
}
