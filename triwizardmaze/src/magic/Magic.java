/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magic;
import maze.Wizard;
import com.golden.gamedev.util.ImageUtil;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import maze.AbstrObject;
import maze.World;
import navigation.CellPosition;
import game.GameSetting;

/**
 *
 * @author godric
 */
public abstract class Magic extends AbstrObject<CellPosition> {

    Wizard _maker;
    protected int _cost;
    
    public Magic(World world, Wizard mk) {
        super(world);
        _maker = mk;
        _cost = 0;
    }

    public Wizard getMaker() {
        return _maker;
    }
    
    protected void setImage(String path) {
        try {
            BufferedImage img = ImageIO.read(new File(path));
            this.setImage(ImageUtil.resize(img,
                    GameSetting.CHAR_WIDTH, GameSetting.CHAR_HEIGHT));
        } catch (IOException ex) {
            Logger.getLogger(Magic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected boolean isCapable(){
        boolean can = this._maker.getMana() > this._cost;
        if(can) this._maker.setMana(this._maker.getMana() - this._cost);
        return can && !this._maker.getDisarmed();
    }
    
    public abstract boolean castASpell();
}
