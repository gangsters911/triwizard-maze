/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collisions;

import maze.Wizard;
import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.collision.AdvanceCollisionGroup;
import game.Triwizardmaze;
import magic.AttackingMagic;
import magic.Expelliarmus;

/**
 *
 * @author godric
 */
public class AttackMagicHitCharacterCollision extends AdvanceCollisionGroup {

    public AttackMagicHitCharacterCollision() {
        pixelPerfectCollision = true;
    }

    @Override
    public void collided(Sprite spellSprite, Sprite damagedCharacter) {
        maze.Character victim = (maze.Character) damagedCharacter;

        
        if (victim instanceof maze.Wizard
                && spellSprite instanceof AttackingMagic) {

            AttackingMagic spell = (AttackingMagic) spellSprite;
            boolean playerFlag = spell.getMaker().isPlayer();

            if (((Wizard) victim).getName() != spell.getMaker().getName()) {
                /*Under Protego*/
                if (((Wizard) victim).isProtected()) {
                    ((Wizard) victim).setProtection(0);
                    spellSprite.setActive(false);
                    if (playerFlag) {
                        Triwizardmaze.getPlayerInstance().lastHighscore += 15;
                    }
                } else {
                    if (spellSprite instanceof Expelliarmus) {
                        ((Wizard) victim).setDisarmed(true);
                        if (playerFlag) {
                            Triwizardmaze.getPlayerInstance().lastHighscore += 5;
                        }
                    }

                    victim.injure(spell.getDamage());
                    
                    if (playerFlag) {
                        Triwizardmaze.getPlayerInstance().lastHighscore += 25;
                    }

                    if (!victim.isAlive() && spell.getMaker().isPlayer()) {
                        Triwizardmaze.getPlayerInstance().addKill(true);
                        Triwizardmaze.getPlayerInstance().lastHighscore += 100;
                    }

                    spellSprite.setActive(false);
                }
            }
        }
        else if(victim instanceof maze.SimpleMonster 
                && spellSprite instanceof AttackingMagic){
            ((maze.SimpleMonster)victim).hadBeenHit((AttackingMagic)spellSprite);
        }else if(victim instanceof maze.InvincibleMonster 
                && spellSprite instanceof AttackingMagic){
            ((maze.InvincibleMonster)victim).hadBeenHit((AttackingMagic)spellSprite);
        }
    }

}
