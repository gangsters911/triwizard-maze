/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collisions;

import maze.Wizard;
import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.collision.AdvanceCollisionGroup;
import maze.Trap;

/**
 *
 * @author godric
 */
public class WizardHitTrapCollision extends AdvanceCollisionGroup{

    public WizardHitTrapCollision() {
        pixelPerfectCollision = true;
    }

    @Override
    public void collided(Sprite w, Sprite t) {
        ((Wizard)w).injure(((Trap)t).getMinusHealth());
    }
    
}
