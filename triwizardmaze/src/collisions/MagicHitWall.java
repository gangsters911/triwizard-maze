/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collisions;

import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.collision.AdvanceCollisionGroup;

/**
 *
 * @author godric
 */
public class MagicHitWall extends AdvanceCollisionGroup{

    public MagicHitWall() {
        pixelPerfectCollision = true;
    }

    @Override
    public void collided(Sprite mg, Sprite wall) {
        mg.setActive(false);
    }
}
